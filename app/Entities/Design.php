<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
    protected $table = 'designs';
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Entities\Category', 'category_id', 'id');
    }

    public function subCategory()
    {
        return $this->belongsTo('App\Entities\SubCategory', 'sub_category_id', 'id');
    }
}
