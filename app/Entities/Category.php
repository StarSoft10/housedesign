<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $guarded = [];

    public function subCategories()
    {
        return $this->hasMany('App\Entities\SubCategory', 'category_id', 'id')->where('blocked','=', 0);
    }

    public function designs()
    {
        return $this->hasMany('App\Entities\Design', 'category_id', 'id')->where('blocked','=', 0);
    }
}
