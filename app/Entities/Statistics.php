<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Statistics extends Model
{
    protected $table = 'statistics';
    protected $guarded = [];
}
