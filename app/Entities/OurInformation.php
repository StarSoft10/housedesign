<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class OurInformation extends Model
{
    protected $table = 'our_information';
    protected $guarded = [];
}
