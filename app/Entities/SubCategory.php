<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_categories';
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Entities\Category', 'category_id', 'id');
    }

    public function designs()
    {
        return $this->hasMany('App\Entities\Design', 'sub_category_id', 'id')->where('blocked','=', 0);
    }
}
