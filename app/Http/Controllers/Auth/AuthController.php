<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signInPost(Request $request)
    {
        $email = !empty($request->email) ? $request->email : '';
        $password = !empty($request->password) ? $request->password : '';

        if($email == '' || $password == ''){
            return back()->with('warning', 'Wrong Credentials');
        }

        if(Auth::attempt(['email' => $email, 'password' => $password])){
            if (Auth::check()){
                $role = Auth::user()->getRoleNames()[0];

                if ($role == 'admin'){
                    return redirect()->route('admin');
                } else {
                    return redirect()->route('signInGet');
                }
            } else {
                return back()->with('warning',  'Wrong Credentials');
            }
        } else {
            return back()->with('warning',  'Wrong Credentials');
        }
    }

    public function logout()
    {
        if(Auth::user()){
            Auth::logout();
        }

        return redirect()->route('home');
    }
}
