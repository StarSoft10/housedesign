<?php

namespace App\Http\Controllers\Front;

use App\Entities\Category;
use App\Entities\ContactUs;
use App\Entities\Design;
use App\Entities\OurInformation;
use App\Entities\Statistics;
use App\Entities\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Stevebauman\Location\Facades\Location;


class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $ip = $this->getIp();
        if ($position = Location::get($ip)) {
            $country = $position->countryName;
        } else {
            $country = 'Unknown';
        }

        if(!Statistics::where('ip', $ip)->where('country', $country)->exists()){
            Statistics::create(['ip' => $ip, 'country' => $country]);
        }

        $categories = Category::where('blocked', 0)->get();

        $ourInformation = OurInformation::first();

        return view('home', compact('categories', 'ourInformation'));
    }

    public function contactUs(Request $request)
    {
        $name = !empty($request->name) ? $request->name : '';
        $phone = !empty($request->phone) ? $request->phone : '';
        $email = !empty($request->email) ? $request->email : '';
        $message = !empty($request->message) ? $request->message : '';

        ContactUs::create(['full_name' => $name, 'phone' => $phone, 'email' => $email, 'message' => $message]);

        return response()->json(['type' => 'success', 'message' => 'Success']);
    }

    public function test()
    {
        $auctions = '';
        return view('test', compact('auctions'));
    }

    public function getIp(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
        return request()->ip(); // it will return server ip when no client ip found
    }
}
