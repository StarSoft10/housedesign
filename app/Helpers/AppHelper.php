<?php

namespace App\Helpers;

use Spatie\Permission\Models\Permission;

class AppHelper
{
    public function getAllowedPermissionsByModel($model)
    {
        if ($model == 'admin') {
            return Permission::where([
                ['name', 'NOT LIKE', 'delete_%'],
            ])->get();
        }

        if ($model == 'customer') {
            return Permission::where([
                ['name', 'LIKE', 'view_%']
            ])->get();
        }
    }
}