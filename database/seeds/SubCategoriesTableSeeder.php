<?php

use App\Entities\SubCategory;
use Illuminate\Database\Seeder;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubCategory::create(['name' => 'Kitchen', 'category_id' => 1]);
        SubCategory::create(['name' => 'Hall', 'category_id' => 1]);
        SubCategory::create(['name' => 'Baby Room', 'category_id' => 1]);

        SubCategory::create(['name' => 'Terrace', 'category_id' => 2]);
        SubCategory::create(['name' => 'Grass', 'category_id' => 2]);
    }
}
