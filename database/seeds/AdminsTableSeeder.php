<?php

use App\Entities\User;
use App\Entities\Admin;
use App\Helpers\AppHelper;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsTableSeeder extends Seeder
{
    public $helper;

    public function __construct()
    {
        $this->helper = new AppHelper();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminPermissions = $this->helper->getAllowedPermissionsByModel('admin');

        /*
        * First
        */
        $admin = User::create([
            'name' => 'Admin nr.1',
            'email' => '1@mail.ru',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123456')
        ]);
        $admin->assignRole('admin');
        foreach ($adminPermissions as $permission) {
            $admin->givePermissionTo($permission->name);
        }
        Admin::create([
            'user_id' => $admin->id,
            'first_name' => 'Admin nr.1',
            'last_name' => 'Admin nr.1',
            'phone' => '123123'
        ]);
    }
}
