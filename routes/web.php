<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;


Route::get('/', function () {
    if (Auth::check()) {
        $role = Auth::user()->getRoleNames()[0];

        if ($role == 'admin') {
            return redirect()->route('admin');
        } else {
            return redirect()->route('home');
        }
    } else {
        return redirect()->route('home');
    }
});

Route::get('/', 'Front\FrontController@index')->name('home');

Route::get('signin', function () {
    return view('auth.signin');
})->name('signInGet');
Route::post('signin', 'Auth\AuthController@signInPost')->name('signInPost');
Route::get('logout', 'Auth\AuthController@logout')->name('logout');
Route::get('/test', 'Front\FrontController@test')->name('test');
Route::post('contact-us', 'Front\FrontController@contactUs')->name('contact-us');
