<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage',
//	'tempDir'               => base_path('../temp/')
    'tempDir'               => public_path('temp'),

    'margin_left'           => 5,
    'margin_right'          => 5,
    'margin_top'            => 5,
    'margin_bottom'         => 5,
    'margin_header'         => 5,
    'margin_footer'         => 5,
];
