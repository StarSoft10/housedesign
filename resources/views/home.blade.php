@extends('layouts.app')

@section('title')
    Interior Design of Houses and Gardens
@endsection

@section('content')
    <section class="section bg-gray-900 box-custom-1">
        <div class="box-custom-1-aside">
            <ul class="box-custom-1-list">
                <li class="wow fadeInLeft" data-wow-delay=".5s">
                    <a class="link link-social" href="@if(!empty($ourInformation)) {{ $ourInformation->facebook_link }} @else javascript:void(0) @endif">
                        <div class="icon novi-icon fa-facebook"></div>
                    </a>
                </li>
                <li class="wow fadeInLeft" data-wow-delay=".7s">
                    <a class="link link-social" href="@if(!empty($ourInformation)) {{ $ourInformation->instagram_link }} @else javascript:void(0) @endif">
                        <div class="icon novi-icon fa-instagram"></div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-custom-1-main" style="background-image: url({{ asset('client/images/home-01-1676x731.webp') }})">
            <div class="block-sm">
                <h2 class="wow fadeInRight" data-wow-delay=".5s">Interior design and decor <br> for your home</h2>
                <p class="wow fadeInRight" data-wow-delay=".6s">Our team creates comfortable spaces for our clients.
                    We’ve been designing your everyday life and work through great ideas since 1999.</p>
            </div>
        </div>
    </section>

    <section class="section section-lg bg-white bg-img-custom" id="about" style="background-image: url({{ asset('client/images/home-16-768x450.webp') }})">
        <div class="container container-custom-left">
            <div class="row justify-content-between row-60 row-md-30 flex-wrap-reverse flex-lg-wrap">
                <div class="col-lg-7">
                    <div class="row row-41">
                        <div class="col-xs-4 maw-29">
                            <div class="row row-41">
                                <div class="col-12">
                                    <div class="box-custom-3 wow fadeInLeft" data-wow-delay=".7s">
                                        <img class="mt-112" src="{{ asset('client/images/home-11-226x261.webp') }}" alt="" width="226" height="261"/>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="box-custom-3 wow fadeInLeft" data-wow-delay=".8s">
                                        <img src="{{ asset('client/images/home-12-226x564.webp') }}" alt="" width="226" height="564"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8">
                            <div class="row row-41">
                                <div class="col-12">
                                    <div class="box-custom-3 wow fadeInLeft" data-wow-delay=".5s">
                                        <img src="{{ asset('client/images/home-13-560x564.webp') }}" alt="" width="560" height="564"/>
                                        <div class="box-custom-3-container">
                                            <div class="box-custom-3-wrap">
                                                <div class="box-custom-3-item">
                                                    <p class="box-custom-3-name">Alice Merton</p>
                                                    <p class="box-custom-3-position">Founder of “Interia”</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="box-custom-3 box-custom-3-border wow fadeInLeft" data-wow-delay=".6s">
                                        <div class="box-custom-3-border-dashed"></div>
                                        <div class="box-custom-3-border-angel"></div>
                                        <img src="{{ asset('client/images/home-14-257x261.webp') }}" alt="" width="257" height="261"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-xxl-4">
                    <div class="box-custom-2">
                        <h2 class="wow fadeInRight" data-wow-delay=".5s">Why Choose Us</h2>
                        <p class="wow fadeInRight" data-wow-delay=".6s">Besides great interior design, there are lots of
                            reasons to choose Interia. Here are some of the most popular ones.
                        </p>
                        <ul class="list index-list list-classic-wrap">
                            <li class="unit unit-spacing-lg align-items-start list-classic flex-column flex-xs-row wow fadeInRight"
                                data-wow-delay=".7s">
                                <div class="unit-left list-index-counter">
                                    <div class="list-classic-rectangle"></div>
                                    <div class="icon novi-icon linearicons-users2 icon-xl"></div>
                                </div>
                                <div class="unit-body">
                                    <h4>Professional Team</h4>
                                    <p>Our team includes only the best decorators and interior designers in the
                                        industry.</p>
                                </div>
                            </li>
                            <li class="unit unit-spacing-lg align-items-start list-classic flex-column flex-xs-row wow fadeInRight"
                                data-wow-delay=".8s">
                                <div class="unit-left list-index-counter">
                                    <div class="list-classic-rectangle"></div>
                                    <div class="icon novi-icon linearicons-lamp icon-lg"></div>
                                </div>
                                <div class="unit-body">
                                    <h4>Unusual Ideas</h4>
                                    <p>Our designers generate various yet always original ideas that will surely fit
                                        your needs.</p>
                                </div>
                            </li>
                            <li class="unit unit-spacing-lg align-items-start list-classic flex-column flex-xs-row wow fadeInRight"
                                data-wow-delay=".9s">
                                <div class="unit-left list-index-counter">
                                    <div class="list-classic-rectangle"></div>
                                    <div class="icon novi-icon linearicons-heart icon-lg"></div>
                                </div>
                                <div class="unit-body">
                                    <h4>Made with Respect</h4>
                                    <p>All our work is built around respect to our clients, great service, and
                                        creativity.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section section-md-2 bg-white pb-0" id="portfolio">
        <div class="bg-custom novi-bg novi-bg-img bg-gray-200"></div>
        <div class="container">
            <div class="row row-30">
                <div class="col-xl-6 col-md-6 wow fadeInLeft" data-wow-delay=".5s">
                    <h2>Our Portfolio</h2>
                </div>
                <div class="col-xl-5 col-md-6 wow fadeInRight" data-wow-delay=".5s">
                    <p style="font-weight: bold;">
                        All our projects are unique and designed to last. Take a look at our recent works to find it out
                        for yourself.
                    </p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">



                <div class="col-md-12 text-center" style="margin-bottom: 20px;">
                    @foreach($categories as $key => $category)
                        <button class="btn @if($key == 0) btn-primary  @else btn-default @endif category-btn" data-category="cat_{{ $category->id }}">
                            {{ $category->name }}
                        </button>
                    @endforeach
                </div>


                @foreach($categories as $key => $category)
                    <div id="cat_{{ $category->id }}" class="col-md-12 category-block @if($key == 0) active-category-block @endif">
                        <div class="col-md-12" style="margin-bottom: 20px">
                            <div align="center">
                                <button class="btn btn-default filter-button active" data-filter="all">All</button>
                                @foreach($category->subCategories as $subCategory)
                                    <button class="btn btn-default filter-button" data-filter="sub_{{ $subCategory->id }}">{{ $subCategory->name }}</button>
                                @endforeach
                            </div>
                        </div>

                        <div class="row">
                            @foreach($category->designs as $design)
                                <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter custom-img-style sub_{{ $design->subCategory->id }}">
                                    <a class="thumbnail fancybox" rel="ligthbox" href="{{ asset('storage/designs/' . $design->photo) }}">
                                        <img class="img-responsive" alt="Photo" src="{{ asset('storage/designs/' . $design->photo) }}"/>
                                    </a>
                                    <ul class="post-classic-description">
                                        <li>{{ $design->name }}</li>
                                    </ul>
                                    <p>{{ $design->description }}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="section section-sm bg-gray-900 novi-bg novi-bg-img pb-0 text-center" id="contacts" style="background-image: url({{ asset('client/images/bg-price-1920x1128.webp') }})">
        <div class="container">
            <div class="row row-fix justify-content-center wow fadeInDown" data-wow-delay=".25s">
                <div class="col-lg-6 col-md-8">
                    <h2>Book a free designer consultation</h2>
                    <p class="pretitle text-primary">A great way to your new interior</p>
                </div>
            </div>
            <div class="row row-50 justify-content-center">
                <div class="col-md-4">
                    <div class="box-classic wow fadeInLeft" data-wow-delay=".5s">
                        <div class="box-classic-icon">
                            <div class="icon novi-icon icon-xl linearicons-bubble-text"></div>
                        </div>
                        <div class="box-classic-text">
                            <p class="subtitle">Discuss every aspect of your interior with one of our designers</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-classic wow fadeInDown" data-wow-delay=".5s">
                        <div class="box-classic-icon">
                            <div class="icon novi-icon icon-xl linearicons-calculator"></div>
                        </div>
                        <div class="box-classic-text">
                            <p class="subtitle">Let us plan and calculate every part of your new interior</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-classic wow fadeInRight" data-wow-delay=".5s">
                        <div class="box-classic-icon">
                            <div class="icon novi-icon icon-xl linearicons-star"></div>
                        </div>
                        <div class="box-classic-text">
                            <p class="subtitle">You can also ask questions and suggest any project changes</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-fix align-items-center mt-112">
                <div class="col-12">
                    <div class="box-cta bg-gray-700 novi-bg">
                        <div class="row row-30">
                            <div class="col-xl-10">
                                <form class="rd-form rd-form-inline" data-form-output="form-output-global"
                                      data-form-type="subscribe">
                                    <div class="form-wrap">
                                        <input class="form-input" id="contact-2-name" type="text" name="name">
                                        <label class="form-label" for="contact-2-name">Full Name</label>
                                    </div>
                                    <div class="form-wrap">
                                        <input class="form-input" id="contact-2-phone" type="text" name="phone">
                                        <label class="form-label" for="contact-2-phone">Phone</label>
                                    </div>
                                    <div class="form-wrap">
                                        <input class="form-input" id="contact-2-email" type="email" name="email">
                                        <label class="form-label" for="contact-2-email">E-mail</label>
                                    </div>
                                    <div class="form-wrap" style="width: 900px;">
                                        <textarea class="form-input" id="contact-2-message" name="message" style="resize: none; height: 100px;overflow: hidden"></textarea>
                                        <label class="form-label" for="contact-2-message">Message</label>
                                    </div>
                                    <div class="form-wrap" style="width: 900px;display: none;font-weight: bold;font-size: 19px;" id="display_message"></div>
                                    <div class="form-button">
                                        <button class="button button-primary button-shadow" type="button" id="send">Send</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xl-2">
                                <div class="box-cta-text">
                                    <p class="small">or call us now</p>
                                    <h4>
                                        <a href="tel:@if(!empty($ourInformation)) {{ $ourInformation->phone }} @else javascript:void(0) @endif">
                                            @if(!empty($ourInformation)) {{ $ourInformation->phone }} @endif
                                        </a>
                                    </h4>
                                    <h4>
                                        <a href="tel:@if(!empty($ourInformation)) {{ $ourInformation->phone2 }} @else javascript:void(0) @endif">
                                            @if(!empty($ourInformation)) {{ $ourInformation->phone2 }} @endif
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('custom-css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.fancybox.min.css') }}">
    {{--<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">--}}
    <style>
        .filter-button.active {
            background-color: #42B32F;
            color: white;
        }
        .custom-img-style {
            margin-bottom: 20px;
            /*border: 2px solid red;*/
        }

        .category-block{
            display: none;
        }

        .active-category-block{
            display: block!important;
        }

        .btn-default {
            background: #ddd6d6;
            border-color: #ddd6d6;
        }

        /*.misa {*/
            /*background-image: url(http://im.tech2.in.com/gallery/2012/dec/stockimage_070930177527_640x360.jpg);*/
            /*!*height: 400px;*!*/
            /*!*width: 400px;*!*/
            /*width: inherit;*/
            /*height: inherit;*/
            /*position: relative;*/
        /*}*/

        /*.misa:after {*/
            /*content: "";*/
            /*display: block;*/
            /*height: 100%;*/
            /*width: 100%;*/
            /*opacity: 0;*/
            /*background: rgba(0,0,0,.5);*/
            /*-moz-transition: all 1s;*/
            /*-webkit-transition: all 1s;*/
            /*transition: all 1s;*/
            /*top: 0;*/
            /*left: 0;*/
            /*position: absolute;*/
        /*}*/

        /*.misa:hover:after {*/
            /*opacity: 1;*/
        /*}*/

        /*.misa p {*/
            /*color: #fff;*/
            /*z-index: 1;*/
            /*position: relative;*/
        /*}*/
    </style>
@endsection

@section('custom-scripts')
    <script type="text/javascript" src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>

    <script>
        let body = $('body');

        $(function () {
            $('.fancybox').fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });
        });

        body.on('click', '.category-btn', function(){
            let dataCategory = $(this).data('category');
            let category = $('#' + dataCategory);

            $('.category-btn').removeClass('btn-primary').addClass('btn-default');
            $(this).addClass('btn-primary').removeClass('btn-default');
            $('.category-block').removeClass('active-category-block');
            category.addClass('active-category-block');
        });

        body.on('click', '.filter-button', function(){
            let value = $(this).attr('data-filter');
            let parent = $(this).parents('.category-block');
            let filterImg = parent.find('.filter');

            parent.find('.filter-button').removeClass('active');
            $(this).addClass('active');

            if(value === 'all'){
                filterImg.show('1000');
            } else {
                filterImg.not('.'+value).hide('3000');
                filterImg.filter('.'+value).show('3000');
            }
        });

        body.on('click', '#send', function () {
            let name = $.trim($('#contact-2-name').val());
            let phone = $.trim($('#contact-2-phone').val());
            let email = $.trim($('#contact-2-email').val());
            let message = $.trim($('#contact-2-message').val());
            let displayMessage = $('#display_message');

            if(name === ''){
                displayMessage.css('color', '#ff0000');
                displayMessage.show().html('Please fill your name.');
                return;
            }
            if(phone === '' && email === ''){
                displayMessage.css('color', '#ff0000');
                displayMessage.show().html('Please fill email or phone.');
                return;
            }

            displayMessage.hide().html('');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('contact-us')}}',
                type: 'POST',
                data: {
                    name: name,
                    phone: phone,
                    email: email,
                    message: message
                },
                success: function(response){
                    displayMessage.css('color', '#04ff00');
                    displayMessage.show().html(response.message);

                    setTimeout(function () {
                        displayMessage.hide().html('');
                    }, 3000)
                }
            });
        });
    </script>
@endsection