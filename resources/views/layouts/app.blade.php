<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title', '')</title>
    <meta name="keywords" content="Interior Design, Home Design, Design, Garden Design, United Kingdom, House Design"/>
    <meta name="description" content="Interior Design, Home Design, Design, Garden Design, United Kingdom, House Design"/>
    <meta property="og:title" content="Interior Design, Home Design, Design, Garden Design, United Kingdom, House Design">
    <meta property="og:description" content="Interior Design, Home Design, Design, Garden Design, United Kingdom, House Design">
    {{--<meta property="og:image" content="https://yorkrefrigerent.md//storage/pages/January2021/LeVU5sTK9cgiU5rL1jix-cropped.jpg">--}}
    {{--<meta property="og:image:width" content="1200">--}}
    {{--<meta property="og:image:height" content="650">--}}
    <meta property="og:site_name" content="yorkrefrigerent.md">

    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Montserrat:400,400i,500,700">
    <link rel="stylesheet" type="text/css" href="{{ asset('client/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('client/css/fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('client/css/style.css') }}">

    @yield('custom-css')
</head>
<body>

<div class="preloader">
    <div class="preloader-body">
        <div class="cssload-container">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Loading...</p>
    </div>
</div>
<div class="page">
@include('layouts.header')

@yield('content')

@include('layouts.footer')
</div>

<div class="snackbars" id="form-output-global"></div>
<script type="text/javascript" src="{{ asset('client/js/core.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('client/js/script.js') }}"></script>
@yield('custom-scripts')
</body>
</html>
