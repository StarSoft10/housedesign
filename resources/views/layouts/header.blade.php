<header class="section page-header context-dark" id="home">
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed"
             data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static"
             data-xxl-layout="rd-navbar-static" data-md-device-layout="rd-navbar-fixed"
             data-lg-device-layout="rd-navbar-fixed" data-xl-device-layout="rd-navbar-static"
             data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px"
             data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true"
             data-xxl-stick-up="true">
            <div class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
            <div class="rd-navbar-main-outer">
                <div class="rd-navbar-main">
                    <div class="rd-navbar-nav-wrap">
                        <ul class="rd-navbar-nav">
                            <li class="rd-nav-item active">
                                <a class="rd-nav-link" href="#home">Home</a>
                            </li>
                            <li class="rd-nav-item">
                                <a class="rd-nav-link" href="#about">About</a>
                            </li>
                            <li class="rd-nav-item">
                                <a class="rd-nav-link" href="#portfolio">Portfolio</a>
                            </li>
                            <li class="rd-nav-item">
                                <a class="rd-nav-link" href="#contacts">Contacts</a>
                            </li>
                        </ul>
                    </div>
                    <div class="rd-navbar-panel">
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span>
                        </button>
                        <div class="rd-navbar-brand">
                            <a class="brand" href="javascript:void(0)">
                                <img src="{{ asset('client/images/logo-default-139x35.webp') }}" alt="" width="139" height="35"/>
                            </a>
                        </div>
                    </div>
                    <div class="rd-navbar-collapse">
                        <ul class="list-inline list-inline-md">
                            <li>
                                <a class="link-bd-btm" href="mailto:@if(!empty($ourInformation)) {{ $ourInformation->email }} @else javascript:void(0) @endif">
                                    @if(!empty($ourInformation)) {{ $ourInformation->email }} @endif
                                </a>
                            </li>
                            <li>
                                <a class="link-bd-btm" href="tel:@if(!empty($ourInformation)) {{ $ourInformation->phone }} @else javascript:void(0) @endif">
                                    @if(!empty($ourInformation)) {{ $ourInformation->phone }} @endif
                                </a>
                            </li>
                            <li>
                                <a class="link-bd-btm" href="tel:@if(!empty($ourInformation)) {{ $ourInformation->phone2 }} @else javascript:void(0) @endif">
                                    @if(!empty($ourInformation)) {{ $ourInformation->phone2 }} @endif
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>