<footer class="section footer-classic bg-gray-900 novi-bg novi-bg-img"
        style="background-image: url({{ asset('client/images/bg-price-1920x1128.webp') }})">
    <div class="container">
        <div class="row row-30 justify-content-between">
            <div class="col-xl-3 col-lg-3 col-sm-6">
                <p class="footer-classic-title">Our Address</p>
                <ul class="footer-classic-list">
                    <li>@if(!empty($ourInformation)) {{ $ourInformation->address }} @endif</li>
                </ul>
            </div>
            <div class="col-xl-2 col-lg-2 col-sm-6">
                <p class="footer-classic-title">Socials</p>
                <ul class="footer-classic-list">
                    <li><a href="@if(!empty($ourInformation)) {{ $ourInformation->facebook_link }} @else javascript:void(0) @endif">Facebook</a></li>
                    <li><a href="@if(!empty($ourInformation)) {{ $ourInformation->instagram_link }} @else javascript:void(0) @endif">Instagram</a></li>
                </ul>
            </div>
            <div class="col-xl-2 col-lg-2 col-sm-6">
                <p class="footer-classic-title">phone</p>
                <ul class="footer-classic-list">
                    <li>
                        <a href="tel:@if(!empty($ourInformation)) {{ $ourInformation->phone }} @else javascript:void(0) @endif">
                            @if(!empty($ourInformation)) {{ $ourInformation->phone }} @endif
                        </a>
                    </li>
                    <li>
                        <a href="tel:@if(!empty($ourInformation)) {{ $ourInformation->phone2 }} @else javascript:void(0) @endif">
                            @if(!empty($ourInformation)) {{ $ourInformation->phone2 }} @endif
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-xl-2 col-lg-3 col-sm-6">
                <p class="footer-classic-title">Email</p>
                <ul class="footer-classic-list">
                    <li>
                        <a href="mailto:@if(!empty($ourInformation)) {{ $ourInformation->email }} @else javascript:void(0) @endif">
                            @if(!empty($ourInformation)) {{ $ourInformation->email }} @endif
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="footer-classic-logo-wrap">
            <a class="brand" href="javascript:void(0)">
                <img src="{{ asset('client/images/logo-default-139x35.webp') }}" alt="" width="139" height="35"/>
            </a>
        </div>
        <p class="rights"><span>&copy;&nbsp; </span><span class="copyright-year"></span><span>&nbsp;</span><span>Interia</span><span>. All Rights Reserved</span></p>
    </div>
</footer>