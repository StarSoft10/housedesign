<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group([
    'prefix' => LaravelLocalization::setLocale() . '/admin',
    'middleware' => ['auth', 'role:admin', 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'localize']
], function () {
    Route::get('/', 'AdminController@index')->name('admin');
    Route::post('getStatisticsByCountry', 'AdminController@getStatisticsByCountry')->name('admin.getStatisticsByCountry');


    Route::group(['prefix' => 'categories'], function () {
        Route::post('delete', 'CategoryController@delete')->name('admin.categories.delete');
        Route::post('block', 'CategoryController@block')->name('admin.categories.block');
        Route::post('unblock', 'CategoryController@unblock')->name('admin.categories.unblock');
    });
    Route::resource('categories', 'CategoryController', ['names' => [
        'index' => 'admin.categories.index',
        'store' => 'admin.categories.store',
        'show' => 'admin.categories.show',
        'update' => 'admin.categories.update'
    ]]);


    Route::group(['prefix' => 'sub-categories'], function () {
        Route::post('delete', 'SubCategoryController@delete')->name('admin.sub-categories.delete');
        Route::post('block', 'SubCategoryController@block')->name('admin.sub-categories.block');
        Route::post('unblock', 'SubCategoryController@unblock')->name('admin.sub-categories.unblock');
        Route::post('getSubCategoriesByCategory', 'SubCategoryController@getSubCategoriesByCategory')->name('admin.sub-categories.getSubCategoriesByCategory');
        Route::post('getSubCategoriesByCategoryAndSelect', 'SubCategoryController@getSubCategoriesByCategoryAndSelect')->name('admin.sub-categories.getSubCategoriesByCategoryAndSelect');
    });
    Route::resource('sub-categories', 'SubCategoryController', ['names' => [
        'index' => 'admin.sub-categories.index',
        'store' => 'admin.sub-categories.store',
        'show' => 'admin.sub-categories.show',
        'update' => 'admin.sub-categories.update'
    ]]);


    Route::group(['prefix' => 'designs'], function () {
        Route::post('delete', 'DesignController@delete')->name('admin.designs.delete');
        Route::post('block', 'DesignController@block')->name('admin.designs.block');
        Route::post('unblock', 'DesignController@unblock')->name('admin.designs.unblock');
    });
    Route::resource('designs', 'DesignController', ['names' => [
        'index' => 'admin.designs.index',
        'store' => 'admin.designs.store',
        'show' => 'admin.designs.show',
        'update' => 'admin.designs.update'
    ]]);


    Route::group(['prefix' => 'contact-us'], function () {
        Route::post('delete', 'ContactUsController@delete')->name('admin.contact-us.delete');
    });
    Route::resource('contact-us', 'ContactUsController', ['names' => [
        'index' => 'admin.contact-us.index',
    ]]);


    Route::resource('our-information', 'OurInformationController', ['names' => [
        'index' => 'admin.our-information.index',
        'store' => 'admin.our-information.store',
        'update' => 'admin.our-information.update'
    ]]);
});