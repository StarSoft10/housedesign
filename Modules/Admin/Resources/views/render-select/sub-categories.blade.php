@if(count($subCategories) > 0)
    @foreach($subCategories as $key => $subCategory)
        <option value="{{ $key }}" @if(isset($subCategoryId) && $subCategoryId == $key) selected @endif>{{ $subCategory }}</option>
    @endforeach
@else
    <option value="0">Not found any sub categories</option>
@endif