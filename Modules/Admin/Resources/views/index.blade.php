@extends('admin::layouts.app')

@section('title')
    Dashboard
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6">
            <figure class="highcharts-figure">
                <div class="buttons">
                    <button type="button" class="btn btn-primary get-statistics active-button-primary" data-period="today">Today</button>
                    <button type="button" class="btn btn-primary get-statistics" data-period="week">Week</button>
                    <button type="button" class="btn btn-primary get-statistics" data-period="month">Month</button>
                    <button type="button" class="btn btn-primary get-statistics" data-period="year">Year</button>
                </div>
                <div id="container"></div>
                {{--<p class="highcharts-description">--}}
                    {{--AAAAAA--}}
                {{--</p>--}}
            </figure>
        </div>

        <div class="col-md-6">
            6
        </div>
    </div>
@endsection

@section('custom-css')
    <style>
        .active-button-primary {
            background-color: #0069d9!important;
            color: #ffffff!important;
        }
        .active-button-primary:hover{
            background-color: #0069d9!important;
            color: #ffffff!important;
        }

        .highcharts-figure,
        .highcharts-data-table table {
            min-width: 320px;
            max-width: 800px;
            /*margin: 1em auto;*/
        }

        .highcharts-data-table table {
            font-family: Verdana, sans-serif;
            border-collapse: collapse;
            border: 1px solid #ebebeb;
            margin: 10px auto;
            text-align: center;
            width: 100%;
            max-width: 500px;
        }

        .highcharts-data-table caption {
            padding: 1em 0;
            font-size: 1.2em;
            color: #555;
        }

        .highcharts-data-table th {
            font-weight: 600;
            padding: 0.5em;
        }

        .highcharts-data-table td,
        .highcharts-data-table th,
        .highcharts-data-table caption {
            padding: 0.5em;
        }

        .highcharts-data-table thead tr,
        .highcharts-data-table tr:nth-child(even) {
            background: #f8f8f8;
        }

        .highcharts-data-table tr:hover {
            background: #f1f7ff;
        }

        input[type="number"] {
            min-width: 50px;
        }

    </style>
@endsection

@section('custom-scripts')
    <script src="{{ asset('js/highcharts.js') }}"></script>
    <script>
        let body = $('body');

        Highcharts.chart('container', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Statistics by country'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Clients',
                colorByPoint: true,
                data: [
                    @foreach($statistics as $key => $total)
                    {
                        name: '{{ $key }}',
                        y: parseInt('{{ $total }}')
                    },
                    @endforeach

                ]
            }]
        });

        body.on('click', '.get-statistics', function () {
            const period = $(this).attr('data-period');
            $('.get-statistics').removeClass('active-button-primary');
            $(this).addClass('active-button-primary');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.getStatisticsByCountry') }}',
                type: 'POST',
                data: {
                    period: period
                },
                success: function (response) {
                    let result = response.data;

                    const chartPie = $('#container').highcharts();

                    chartPie.series[0].update({
                        data: result
                    }, false);
                    chartPie.redraw();

                }
            });
        });
    </script>
@endsection
