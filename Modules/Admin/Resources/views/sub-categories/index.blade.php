@extends('admin::layouts.app')

@section('title')
   Sub Categories
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title">Sub Categories</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addSubCategory">
                                Add sub category
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body table-full-width table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($subCategories) > 0)
                                @foreach($subCategories as $subCategory)
                                    <tr @if($subCategory->blocked) class="blocked" @endif>
                                        <td>{{ $subCategory->id }}</td>
                                        <td>{{ $subCategory->name }}</td>
                                        <td>{{ $subCategory->category->name }}</td>
                                        <td>
                                            <a href="{{ route('admin.sub-categories.show', $subCategory->id) }}" class="btn-no-bg" title="Show/Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-subcategoryid="{{ $subCategory->id }}" class="btn-no-bg delete-subcategory" title="Delete">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            @if($subCategory->blocked)
                                                <a href="javascript:void(0)" data-subcategoryid="{{ $subCategory->id }}" class="btn-no-bg unblock-subcategory" title="Unblock">
                                                    <i class="fa fa-unlock"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" data-subcategoryid="{{ $subCategory->id }}" class="btn-no-bg block-subcategory" title="Block">
                                                    <i class="fa fa-lock"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="4">Not found any sub categories.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $subCategories->render() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addSubCategory" role="dialog" aria-labelledby="addSubCategory" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('admin.sub-categories.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <div class="modal-title">Add sub category</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Category', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="category_id" id="category_id" title="Category">
                                        @foreach($categories as $key => $category)
                                            <option value="{{ $key }}">{{ $category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteSubCategory" role="dialog" aria-labelledby="deleteSubCategory" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.sub-categories.delete']) !!}
                    <div class="modal-header">
                        <div class="modal-title">Delete category</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                Are you sure to delete sub category?
                            </div>
                        </div>
                        <input type="hidden" name="sub_category_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="unblockSubCategory" role="dialog" aria-labelledby="unblockSubCategory" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.sub-categories.unblock']) !!}
                <div class="modal-header">
                    <div class="modal-title">Unblock sub category</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        <div class="alert-text">
                            Are you sure to unblock sub category?
                        </div>
                    </div>
                    <input type="hidden" name="sub_category_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Unblock</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="blockSubCategory" role="dialog" aria-labelledby="blockSubCategory" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.sub-categories.block']) !!}
                <div class="modal-header">
                    <div class="modal-title">Block sub category</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        <div class="alert-text">
                            Are you sure to block sub category?
                        </div>
                    </div>
                    <input type="hidden" name="sub_category_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning">Block</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <style>
        .modal {
            top: -72px!important;
        }
        .modal-title {
            font-size: 20px!important;
        }
        .blocked {
            background-color: #9b9999!important;
        }
    </style>
@endsection

@section('custom-scripts')

    <script>
        let body = $('body');

        body.on('click', '.delete-subcategory', function () {
            let subCategoryId = $(this).attr('data-subcategoryid');
            let modal = $('#deleteSubCategory');
            modal.find('input[name="sub_category_id"]').val(subCategoryId);
            modal.modal('show');
        });

        body.on('click', '.unblock-subcategory', function () {
            let subCategoryId = $(this).attr('data-subcategoryid');
            let modal = $('#unblockSubCategory');
            modal.find('input[name="sub_category_id"]').val(subCategoryId);
            modal.modal('show');
        });

        body.on('click', '.block-subcategory', function () {
            let subCategoryId = $(this).attr('data-subcategoryid');
            let modal = $('#blockSubCategory');
            modal.find('input[name="sub_category_id"]').val(subCategoryId);
            modal.modal('show');
        });

        $('#deleteSubCategory').on('hidden.bs.modal', function () {
            $(this).find('input[name="sub_category_id"]').val('');
        });
        $('#unblockSubCategory').on('hidden.bs.modal', function () {
            $(this).find('input[name="sub_category_id"]').val('');
        });
        $('#blockSubCategory').on('hidden.bs.modal', function () {
            $(this).find('input[name="sub_category_id"]').val('');
        });
    </script>
@endsection