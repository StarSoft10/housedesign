@extends('admin::layouts.app')

@section('title')
    Sub Category: {{ $subCategory->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">Sub Category information</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;">
                        <li><span>Name:</span><b>&nbsp;{{ $subCategory->name }}</b></li>
                        <li><span>Category:</span><b>&nbsp;{{ $subCategory->category->name }}</b></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit"></i>
                    <strong class="card-title">Edit sub category</strong>
                </div>
                <div class="card-body">
                    {!! Form::model($subCategory, ['method' => 'PATCH', 'route' => ['admin.sub-categories.update', $subCategory->id], 'autocomplete' => 'off']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Category', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="category_id" id="category_id">
                                        @foreach($categories as $key => $category)
                                            <option value="{{ $key }}" @if($key == $subCategory->category_id) selected @endif>{{ $category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        {!! Form::submit('Edit', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
@endsection
