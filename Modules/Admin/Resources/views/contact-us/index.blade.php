@extends('admin::layouts.app')

@section('title')
   Contact us
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title">Contact us</h4>
                        </div>
                    </div>
                </div>

                <div class="card-body table-full-width table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Full Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Message</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($contactUs) > 0)
                                @foreach($contactUs as $contact)
                                    <tr>
                                        <td>{{ $contact->id }}</td>
                                        <td>{{ $contact->full_name }}</td>
                                        <td>{{ $contact->email }}</td>
                                        <td>{{ $contact->phone }}</td>
                                        <td>{{ $contact->message }}</td>
                                        <td>
                                            <a href="javascript:void(0)" data-contactusid="{{ $contact->id }}" class="btn-no-bg delete-contact-us" title="Delete">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="6">Not found any contact us.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $contactUs->render() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteContactUs" role="dialog" aria-labelledby="deleteContactUs" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.contact-us.delete']) !!}
                    <div class="modal-header">
                        <div class="modal-title">Delete contact us</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                Are you sure to delete contact us?
                            </div>
                        </div>
                        <input type="hidden" name="contact_us_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <style>
        .modal {
            top: -160px!important;
        }
        .modal-title {
            font-size: 20px!important;
        }
    </style>
@endsection

@section('custom-scripts')

    <script>
        let body = $('body');

        body.on('click', '.delete-contact-us', function () {
            let contactUsId = $(this).attr('data-contactusid');
            let modal = $('#deleteContactUs');
            modal.find('input[name="contact_us_id"]').val(contactUsId);
            modal.modal('show');
        });

        $('#deleteContactUs').on('hidden.bs.modal', function () {
            $(this).find('input[name="contact_us_id"]').val('');
        });
    </script>
@endsection