@extends('admin::layouts.app')

@section('title')
    Design: {{ $design->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-info-circle"></i>
                    <strong class="card-title">Design information</strong>
                </div>
                <div class="card-body">
                    <ul style="list-style: none;padding: 0">
                        <li><span>Name:</span><b>&nbsp;{{ $design->name }}</b></li>
                        <li><span>Category:</span><b>&nbsp;{{ $design->category->name }}</b></li>
                        <li><span>SubCategory:</span><b>&nbsp;{{ $design->subCategory->name }}</b></li>
                        <li><span>Description:</span><b>&nbsp;{{ $design->description }}</b></li>
                    </ul>
                    <div>
                        @if($design->photo == null || $design->photo == '')
                            <img class="rounded-circle mx-auto" src="{{ asset('images/no_image.png') }}" alt="Photo" style="display: block;width: 100%;">
                        @else
                            <img src="{{ asset('storage/designs/' . $design->photo) }}" alt="Photo" style="display: block;width: 100%;">
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-edit"></i>
                    <strong class="card-title">Edit design</strong>
                </div>
                <div class="card-body">
                    {!! Form::model($design, ['method' => 'PATCH', 'route' => ['admin.designs.update', $design->id], 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) !!}
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Category', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="category_id" id="category_id" title="">
                                        @foreach($categories as $key => $category)
                                            <option value="{{ $key }}" @if($key == $design->category_id) selected @endif>{{ $category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('sub_category_id', 'Sub Category', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="sub_category_id" id="sub_category_id" title="">
                                        <option value="0">Choose category</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="photo">Change file</label>
                                    <input type="file" class="form-control-file" id="photo" name="photo" accept="image/*">
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                                    <textarea name="description" class="form-control" style="height: 111px;" title="">{{ $design->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        {!! Form::submit('Edit', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
@endsection

@section('custom-scripts')
    <script>
        let body = $('body');

        $(function () {
            let categoryId = $.trim($('#category_id').val());
            let subCategorySelect = $('#sub_category_id');
            let subCategoryId = '{{ $subCategoryId }}';
            if(categoryId === '' || categoryId === undefined || categoryId == null){
                return;
            }
            getSubCategoriesByCategoryAndSelect(categoryId, subCategorySelect, subCategoryId);
        });

        body.on('change', '#category_id', function () {
            let categoryId = $.trim($(this).val());
            let subCategorySelect = $('#sub_category_id');

            if(categoryId === '' || categoryId === undefined || categoryId == null){
                return;
            }

            getSubCategoriesByCategory(categoryId, subCategorySelect);
        });

        function getSubCategoriesByCategoryAndSelect(categoryId, subCategorySelect, subCategoryId) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.sub-categories.getSubCategoriesByCategoryAndSelect')}}',
                type: 'POST',
                data: {
                    categoryId: categoryId,
                    subCategoryId: subCategoryId
                },
                success: function(response){
                    subCategorySelect.html(response.html);
                }
            });
        }


        function getSubCategoriesByCategory(categoryId, subCategorySelect) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.sub-categories.getSubCategoriesByCategory')}}',
                type: 'POST',
                data: {
                    categoryId: categoryId
                },
                success: function(response){
                    subCategorySelect.html(response.html);
                }
            });
        }
    </script>
@endsection
