@extends('admin::layouts.app')

@section('title')
   Designs
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title">Designs</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addDesign">
                                Add design
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body table-full-width table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Sub Category</th>
                                <th>Description</th>
                                <th>Photo</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($designs) > 0)
                                @foreach($designs as $design)
                                    <tr @if($design->blocked) class="blocked" @endif>
                                        <td>{{ $design->id }}</td>
                                        <td>{{ $design->name }}</td>
                                        <td>{{ $design->category->name }}</td>
                                        <td>{{ $design->subCategory->name }}</td>
                                        <td>{{ \Illuminate\Support\Str::limit($design->description, 40, $end='...') }}</td>
                                        <td>
                                            @if($design->photo == null || $design->photo == '')
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ asset('images/no_image.png') }}">
                                                    <img class="img-responsive" alt="No Photo" src="{{ asset('images/no_image.png') }}" style="width: 50px;height: 50px;"/>
                                                </a>
                                            @else
                                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ asset('storage/designs/' . $design->photo) }}">
                                                    <img class="img-responsive" alt="Photo" src="{{ asset('storage/designs/' . $design->photo) }}" style="width: 50px;height: 50px;"/>
                                                </a>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.designs.show', $design->id) }}" class="btn-no-bg" title="Show/Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-designid="{{ $design->id }}" class="btn-no-bg delete-design" title="Delete">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            @if($design->blocked)
                                                <a href="javascript:void(0)" data-designid="{{ $design->id }}" class="btn-no-bg unblock-design" title="Unblock">
                                                    <i class="fa fa-unlock"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" data-designid="{{ $design->id }}" class="btn-no-bg block-design" title="Block">
                                                    <i class="fa fa-lock"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="7">Not found any designs.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $designs->render() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addDesign" role="dialog" aria-labelledby="addDesign" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('admin.designs.store'), 'method' => 'post', 'autocomplete' => 'off', 'enctype' => 'multipart/form-data']) !!}
                    <div class="modal-header">
                        <div class="modal-title">Add design</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('category_id', 'Category', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="category_id" id="category_id" title="Category">
                                        @foreach($categories as $key => $category)
                                            <option value="{{ $key }}">{{ $category }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('sub_category_id', 'Sub Category', ['class' => 'control-label']) !!}
                                    <select class="form-control" name="sub_category_id" id="sub_category_id" title="">
                                        <option value="0">Choose category</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="photo">Choose file</label>
                                    <input type="file" class="form-control-file" id="photo" name="photo" accept="image/*" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
                                    <textarea name="description" class="form-control" style="height: 111px;" title=""></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteDesign" role="dialog" aria-labelledby="deleteDesign" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.designs.delete']) !!}
                    <div class="modal-header">
                        <div class="modal-title">Delete category</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                Are you sure to delete design?
                            </div>
                        </div>
                        <input type="hidden" name="design_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="unblockDesign" role="dialog" aria-labelledby="unblockDesign" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.designs.unblock']) !!}
                <div class="modal-header">
                    <div class="modal-title">Unblock design</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        <div class="alert-text">
                            Are you sure to unblock design?
                        </div>
                    </div>
                    <input type="hidden" name="design_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Unblock</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="blockDesign" role="dialog" aria-labelledby="blockDesign" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.designs.block']) !!}
                <div class="modal-header">
                    <div class="modal-title">Block design</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        <div class="alert-text">
                            Are you sure to block design?
                        </div>
                    </div>
                    <input type="hidden" name="design_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning">Block</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
<style>
    #addDesign {
        top: -148px!important;
    }
    .modal-title {
        font-size: 20px!important;
    }
    .blocked {
        background-color: #9b9999!important;
    }
</style>
@endsection

@section('custom-scripts')

    <script>
        $(function () {
            $('.fancybox').fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });

            $('#addDesign').on('show.bs.modal', function () {
                let categoryId = $.trim($('#category_id').val());
                let subCategorySelect = $('#sub_category_id');
                if(categoryId === '' || categoryId === undefined || categoryId == null){
                    return;
                }
                getSubCategoriesByCategory(categoryId, subCategorySelect);
            });
        });

        let body = $('body');

        body.on('click', '.delete-design', function () {
            let designId = $(this).attr('data-designid');
            let modal = $('#deleteDesign');
            modal.find('input[name="design_id"]').val(designId);
            modal.modal('show');
        });

        body.on('click', '.unblock-design', function () {
            let designId = $(this).attr('data-designid');
            let modal = $('#unblockDesign');
            modal.find('input[name="design_id"]').val(designId);
            modal.modal('show');
        });

        body.on('click', '.block-design', function () {
            let designId = $(this).attr('data-designid');
            let modal = $('#blockDesign');
            modal.find('input[name="design_id"]').val(designId);
            modal.modal('show');
        });

        $('#deleteDesign').on('hidden.bs.modal', function () {
            $(this).find('input[name="design_id"]').val('');
        });
        $('#unblockDesign').on('hidden.bs.modal', function () {
            $(this).find('input[name="design_id"]').val('');
        });
        $('#blockDesign').on('hidden.bs.modal', function () {
            $(this).find('input[name="design_id"]').val('');
        });

        body.on('change', '#category_id', function () {
            let categoryId = $.trim($(this).val());
            let subCategorySelect = $('#sub_category_id');

            if(categoryId === '' || categoryId === undefined || categoryId == null){
                return;
            }

            getSubCategoriesByCategory(categoryId, subCategorySelect);
        });

        function getSubCategoriesByCategory(categoryId, subCategorySelect) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('admin.sub-categories.getSubCategoriesByCategory')}}',
                type: 'POST',
                data: {
                    categoryId: categoryId
                },
                success: function(response){
                    subCategorySelect.html(response.html);
                }
            });
        }
    </script>
@endsection