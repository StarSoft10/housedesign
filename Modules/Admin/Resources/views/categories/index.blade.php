@extends('admin::layouts.app')

@section('title')
   Categories
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="card-title">Categories</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addCategory">
                                Add category
                            </button>
                        </div>
                    </div>
                </div>

                <div class="card-body table-full-width table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($categories) > 0)
                                @foreach($categories as $category)
                                    <tr @if($category->blocked) class="blocked" @endif>
                                        <td>{{ $category->id }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                            <a href="{{ route('admin.categories.show', $category->id) }}" class="btn-no-bg" title="Show/Edit">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" data-categoryid="{{ $category->id }}" class="btn-no-bg delete-category" title="Delete">
                                                <i class="fa fa-trash-o"></i>
                                            </a>
                                            @if($category->blocked)
                                                <a href="javascript:void(0)" data-categoryid="{{ $category->id }}" class="btn-no-bg unblock-category" title="Unblock">
                                                    <i class="fa fa-unlock"></i>
                                                </a>
                                            @else
                                                <a href="javascript:void(0)" data-categoryid="{{ $category->id }}" class="btn-no-bg block-category" title="Block">
                                                    <i class="fa fa-lock"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center">
                                    <td colspan="3">Not found any categories.</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    {!! $categories->render() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="addCategory" role="dialog" aria-labelledby="addCategory" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['url' => route('admin.categories.store'), 'method' => 'post', 'autocomplete' => 'off']) !!}
                    <div class="modal-header">
                        <div class="modal-title">Add category</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                                    {!! Form::text('name', '', ['class' => 'form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteCategory" role="dialog" aria-labelledby="deleteCategory" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.categories.delete']) !!}
                    <div class="modal-header">
                        <div class="modal-title">Delete category</div>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-info" role="alert">
                            <div class="alert-text">
                                Are you sure to delete category?
                            </div>
                        </div>
                        <input type="hidden" name="category_id">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="unblockCategory" role="dialog" aria-labelledby="unblockCategory" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.categories.unblock']) !!}
                <div class="modal-header">
                    <div class="modal-title">Unblock category</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        <div class="alert-text">
                            Are you sure to unblock category?
                        </div>
                    </div>
                    <input type="hidden" name="category_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Unblock</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="blockCategory" role="dialog" aria-labelledby="blockCategory" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                {!! Form::open(['route' => 'admin.categories.block']) !!}
                <div class="modal-header">
                    <div class="modal-title">Block category</div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                        <div class="alert-text">
                            Are you sure to block category?
                        </div>
                    </div>
                    <input type="hidden" name="category_id">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-warning">Block</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('custom-css')
    <style>
        .modal {
            top: -45px!important;
        }
        .modal-title {
            font-size: 20px!important;
        }
        .blocked {
            background-color: #9b9999!important;
        }
    </style>
@endsection

@section('custom-scripts')

    <script>
        let body = $('body');

        body.on('click', '.delete-category', function () {
            let categoryId = $(this).attr('data-categoryid');
            let modal = $('#deleteCategory');
            modal.find('input[name="category_id"]').val(categoryId);
            modal.modal('show');
        });

        body.on('click', '.unblock-category', function () {
            let categoryId = $(this).attr('data-categoryid');
            let modal = $('#unblockCategory');
            modal.find('input[name="category_id"]').val(categoryId);
            modal.modal('show');
        });

        body.on('click', '.block-category', function () {
            let categoryId = $(this).attr('data-categoryid');
            let modal = $('#blockCategory');
            modal.find('input[name="category_id"]').val(categoryId);
            modal.modal('show');
        });

        $('#deleteCategory').on('hidden.bs.modal', function () {
            $(this).find('input[name="category_id"]').val('');
        });
        $('#unblockCategory').on('hidden.bs.modal', function () {
            $(this).find('input[name="category_id"]').val('');
        });
        $('#blockCategory').on('hidden.bs.modal', function () {
            $(this).find('input[name="category_id"]').val('');
        });
    </script>
@endsection