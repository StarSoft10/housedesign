{{--<div class="sidebar" data-image="../assets/img/sidebar-5.jpg">--}}
<div class="sidebar" data-color="blue">

{{--Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"--}}
{{--Tip 2: you can also add an image using data-image tag--}}

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ route('admin') }}" class="simple-text">
                Luta Alexandru
            </a>
        </div>
        <ul class="nav">
            <li class="nav-item @if(Request::is('*admin')) active @endif">
                <a class="nav-link" href="{{ route('admin') }}">
                    <i class="nc-icon nc-chart-pie-35"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item @if(Request::is('*/categories*')) active @endif">
                <a class="nav-link" href="{{ route('admin.categories.index') }}">
                    <i class="nc-icon nc-notes"></i>
                    <p>Categories</p>
                </a>
            </li>
            <li class="nav-item @if(Request::is('*/sub-categories*')) active @endif">
                <a class="nav-link" href="{{ route('admin.sub-categories.index') }}">
                    <i class="nc-icon nc-paper-2"></i>
                    <p>Sub Categories</p>
                </a>
            </li>
            <li class="nav-item @if(Request::is('*/designs*')) active @endif">
                <a class="nav-link" href="{{ route('admin.designs.index') }}">
                    <i class="nc-icon nc-ruler-pencil"></i>
                    <p>Designs</p>
                </a>
            </li>
            <li class="nav-item @if(Request::is('*/contact-us*')) active @endif">
                <a class="nav-link" href="{{ route('admin.contact-us.index') }}">
                    <i class="nc-icon nc-notification-70"></i>
                    <p>Contact us</p>
                </a>
            </li>
            <li class="nav-item @if(Request::is('*/our-information*')) active @endif">
                <a class="nav-link" href="{{ route('admin.our-information.index') }}">
                    <i class="nc-icon nc-paper-2"></i>
                    <p>Our Information</p>
                </a>
            </li>
        </ul>
    </div>
</div>


