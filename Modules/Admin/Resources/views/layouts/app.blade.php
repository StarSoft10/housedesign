<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @yield('title', '')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    {{--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.fancybox.min.css') }}">
    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}"  />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/light-bootstrap-dashboard.css?v=2.0.0') }}" />
    <style> button {cursor: pointer} input[type=submit] {cursor: pointer}</style>
    @yield('custom-css')
</head>
<body>

<div class="wrapper">
    <!-- Sidebar -->
    @include('admin::layouts.sidebar')
    <!-- Sidebar -->

    <div class="main-panel">
        <!-- Navbar -->
        @include('admin::layouts.navbar')
        <!-- Navbar -->

        <div class="content">
            <div class="container-fluid">

                @if (session()->has('error'))
                    <div class="alert alert-danger" style="position: relative;">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                            <i class="nc-icon nc-simple-remove"></i>
                        </button>
                        <span>
                            <b> Error - </b> {{ session()->get('error') }}
                        </span>
                    </div>
                @endif

                @if (session()->has('warning'))
                    <div class="alert alert-warning" style="position: relative;">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                            <i class="nc-icon nc-simple-remove"></i>
                        </button>
                        <span>
                            <b> Warning - </b> {{ session()->get('warning') }}
                        </span>
                    </div>
                @endif

                @if(session()->has('success'))
                    <div class="alert alert-success" style="position: relative;">
                        <button type="button" aria-hidden="true" class="close" data-dismiss="alert">
                            <i class="nc-icon nc-simple-remove"></i>
                        </button>
                        <span>
                            <b> Success - </b> {{ session()->get('success') }}
                        </span>
                    </div>
                @endif

                @yield('content')
            </div>
        </div>

    <!-- Footer -->
    {{--@include('admin::layouts.footer')--}}
    <!-- Footer -->
    </div>
</div>

<script src="{{ asset('assets/js/core/jquery.3.2.1.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/core/popper.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/core/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/plugins/bootstrap-switch.js') }}"></script>

<script src="{{ asset('assets/js/plugins/bootstrap-notify.js') }}"></script>
<script src="{{ asset('assets/js/light-bootstrap-dashboard.js?v=2.0.0') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>

@yield('custom-scripts')
</body>
</html>
