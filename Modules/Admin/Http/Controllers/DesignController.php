<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\Category;
use App\Entities\Design;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;

class DesignController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index()
    {
        $designs = Design::orderBy('id', 'DESC')->paginate(10);
        $categories = Category::where('blocked', 0)->orderBy('id', 'DESC')->pluck('name', 'id');

        return view('admin::designs.index', compact('designs', 'categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:designs',
            'category_id' => 'required|numeric',
            'sub_category_id' => 'required|numeric',
            'photo' => 'required|mimes:jpeg,png,jpg,gif,svg|max:10000'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $photoName = null;
        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $photoName = time() . '.' . $photo->getClientOriginalExtension();
            Storage::disk('public')->put('designs/' . $photoName,  File::get($photo));
        }

        Design::create([
            'category_id' => trim($request->category_id),
            'sub_category_id' => trim($request->sub_category_id),
            'name' => trim($request->name),
            'description' => trim($request->description),
            'photo' => $photoName
        ]);

        return back()->with('success', 'Design created successfully');
    }

    public function show($id)
    {
        $design = Design::where('id', $id)->first();

        if (!$design) {
            return back()->with('warning', 'Design not found');
        }
        $categories = Category::where('blocked', 0)->orderBy('id', 'DESC')->pluck('name', 'id');
        $subCategoryId = $design->sub_category_id;

        return view('admin::designs.show', compact('design', 'categories', 'subCategoryId'));
    }

    public function update(Request $request, $id)
    {
        $design = Design::where('id', $id)->first();
        if (!$design) {
            return back()->with('warning', 'Design not found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('designs')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                })
            ],
            'category_id' => 'required|numeric',
            'sub_category_id' => 'required|numeric',
            'photo' => 'mimes:jpeg,png,jpg,gif,svg|max:10000'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $photoName = $design->photo;
        if ($request->hasFile('photo')) {
            if(Storage::disk('public')->exists('designs/' . $photoName)){
                Storage::disk('public')->delete('designs/' . $photoName);
            }
            $photo = $request->file('photo');
            $photoName = time() . '.' . $photo->getClientOriginalExtension();
            Storage::disk('public')->put('designs/' . $photoName,  File::get($photo));
        }

        Design::where('id', $id)->update([
            'category_id' => trim($request->category_id),
            'sub_category_id' => trim($request->sub_category_id),
            'name' => trim($request->name),
            'description' => trim($request->description),
            'photo' => $photoName,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('admin.designs.index')->with('success', 'Design updated successfully');
    }

    public function block(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'design_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $design = Design::where('id', $request->design_id)->first();
        if (!$design) {
            return back()->with('warning', 'Design not found');
        }

        Design::where('id', $request->design_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Design blocked successfully');
    }

    public function unblock(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'design_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $design = Design::where('id', $request->design_id)->first();
        if (!$design) {
            return back()->with('warning', 'Design not found');
        }

        Design::where('id', $request->design_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Design unblocked successfully');
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'design_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $design = Design::where('id', $request->design_id)->first();
        if (!$design) {
            return back()->with('warning', 'Design not found');
        }

        if(Storage::disk('public')->exists('designs/' . $design->photo)){
            Storage::disk('public')->delete('designs/' . $design->photo);
        }

        $design->delete();

        return back()->with('success', 'Design deleted successfully');
    }
}
