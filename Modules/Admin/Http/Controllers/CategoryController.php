<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\Category;
use App\Entities\Design;
use App\Entities\SubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index()
    {
        $categories = Category::orderBy('id', 'DESC')->paginate(10);

        return view('admin::categories.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Category::create(['name' => trim($request->name)]);

        return back()->with('success', 'Category created successfully');
    }

    public function show($id)
    {
        $category = Category::where('id', $id)->first();

        if (!$category) {
            return back()->with('warning', 'Category not found');
        }

        return view('admin::categories.show', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $category = Category::where('id', $id)->first();
        if (!$category) {
            return back()->with('warning', 'Category not found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('categories')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                })
            ]
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        Category::where('id', $id)->update([
            'name' => trim($request->name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('admin.categories.index')->with('success', 'Category updated successfully');
    }

    public function block(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $category = Category::where('id', $request->category_id)->first();
        if (!$category) {
            return back()->with('warning', 'Category not found');
        }

        Category::where('id', $request->category_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Category blocked successfully');
    }

    public function unblock(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $category = Category::where('id', $request->category_id)->first();
        if (!$category) {
            return back()->with('warning', 'Category not found');
        }

        Category::where('id', $request->category_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Category unblocked successfully');
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $category = Category::where('id', $request->category_id)->first();
        if (!$category) {
            return back()->with('warning', 'Category not found');
        }

        $designs = Design::where('category_id', $request->category_id)->get();
        if(count($designs) > 0){
            foreach ($designs as $design){
                if(Storage::disk('public')->exists('designs/' . $design->photo)){
                    Storage::disk('public')->delete('designs/' . $design->photo);
                }

                $design->delete();
            }
        }

        $subCategories = SubCategory::where('category_id', $request->category_id)->get();
        if(count($subCategories) > 0){
            foreach ($subCategories as $subCategory){
                $subCategory->delete();
            }
        }

        $category->delete();

        return back()->with('success', 'Category deleted successfully');
    }
}
