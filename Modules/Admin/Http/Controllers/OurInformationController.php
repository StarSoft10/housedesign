<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\OurInformation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Routing\Controller;

class OurInformationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index()
    {
        $ourInformation = OurInformation::first();

        $exist = 0;
        if($ourInformation) {
            $exist = 1;
        }

        return view('admin::our-information.index', compact('ourInformation', 'exist'));
    }

    public function store(Request $request)
    {
        OurInformation::create([
            'phone' => trim($request->phone),
            'phone2' => trim($request->phone2),
            'email' => trim($request->email),
            'address' => trim($request->address),
            'facebook_link' => trim($request->facebook_link),
            'instagram_link' => trim($request->instagram_link),
        ]);

        return back()->with('success', 'Information added successfully');
    }

    public function update(Request $request, $id)
    {
        OurInformation::where('id', $id)->update([
            'phone' => trim($request->phone),
            'phone2' => trim($request->phone2),
            'email' => trim($request->email),
            'address' => trim($request->address),
            'facebook_link' => trim($request->facebook_link),
            'instagram_link' => trim($request->instagram_link),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('admin.our-information.index')->with('success', 'Information updated successfully');
    }
}
