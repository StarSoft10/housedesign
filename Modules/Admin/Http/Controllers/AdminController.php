<?php

namespace Modules\Admin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $from = Carbon::now()->format('Y-m-d 00:00:01');
        $to = Carbon::now()->format('Y-m-d 23:59:59');

        $statisticsByCountry = DB::table('statistics')
            ->select(DB::raw('country, COUNT(DISTINCT country, ip) AS total'))
            ->whereBetween('created_at', [$from, $to])
            ->groupBy('country')
            ->get();

        $statistics = [];
        foreach ($statisticsByCountry as $statistic){
            $statistics[$statistic->country] = $statistic->total;
        }

        return view('admin::index', compact('statistics'));
    }

    public function getStatisticsByCountry(Request $request)
    {
        $period = !empty($request->period) ? $request->period : 'today';

        $now = Carbon::now();
        if($period == 'today'){
            $from = $now->format('Y-m-d 00:00:01');
            $to = $now->format('Y-m-d 23:59:59');
        } elseif ($period == 'week'){
            $from = $now->startOfWeek()->format('Y-m-d 00:00:01');
            $to = $now->endOfWeek()->format('Y-m-d  23:59:59');
        } elseif ($period == 'month'){
            $from = $now->startOfMonth()->format('Y-m-d 00:00:01');
            $to = $now->endOfMonth()->format('Y-m-d  23:59:59');
        } elseif ($period == 'year'){
            $from = $now->startOfYear()->format('Y-m-d 00:00:01');
            $to = $now->endOfYear()->format('Y-m-d  23:59:59');
        } else {
            $from = $now->format('Y-m-d 00:00:01');
            $to = $now->format('Y-m-d 23:59:59');
        }

        $statisticsByCountry = DB::table('statistics')
            ->select(DB::raw('country, COUNT(DISTINCT country, ip) AS total'))
            ->whereBetween('created_at', [$from, $to])
            ->groupBy('country')
            ->get();

        $statistics = [];
        foreach ($statisticsByCountry as $total){
            $statistics[$total->country] = $total->total;
        }

        $result = [];
        foreach ($statistics as $key => $total){
            $result[] = [
                'name' => $key,
                'y' => $total
            ];
        }

        return response()->json(['type' => 'success', 'data' => $result]);
    }
}
