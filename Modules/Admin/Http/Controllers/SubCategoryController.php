<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\Category;
use App\Entities\Design;
use App\Entities\SubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Routing\Controller;

class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index()
    {
        $subCategories = SubCategory::orderBy('id', 'DESC')->paginate(10);
        $categories = Category::where('blocked', 0)->orderBy('id', 'DESC')->pluck('name', 'id');

        return view('admin::sub-categories.index', compact('subCategories', 'categories'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:sub_categories',
            'category_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        SubCategory::create(['category_id' => trim($request->category_id), 'name' => trim($request->name)]);

        return back()->with('success', 'Sub category created successfully');
    }

    public function show($id)
    {
        $subCategory = SubCategory::where('id', $id)->first();

        if (!$subCategory) {
            return back()->with('warning', 'Sub category not found');
        }
        $categories = Category::where('blocked', 0)->orderBy('id', 'DESC')->pluck('name', 'id');

        return view('admin::sub-categories.show', compact('subCategory', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $subCategory = SubCategory::where('id', $id)->first();
        if (!$subCategory) {
            return back()->with('warning', 'Sub category not found');
        }

        $validator = Validator::make($request->all(), [
            'name' => [
                'required',
                Rule::unique('sub_categories')->where(function ($query) use ($id) {
                    $query->where('id', '<>', $id);
                })
            ],
            'category_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        SubCategory::where('id', $id)->update([
            'category_id' => trim($request->category_id),
            'name' => trim($request->name),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return redirect()->route('admin.sub-categories.index')->with('success', 'Sub category updated successfully');
    }

    public function block(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sub_category_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $subCategory = SubCategory::where('id', $request->sub_category_id)->first();
        if (!$subCategory) {
            return back()->with('warning', 'Sub category not found');
        }

        SubCategory::where('id', $request->sub_category_id)->update([
            'blocked' => 1,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Sub category blocked successfully');
    }

    public function unblock(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sub_category_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $subCategory = SubCategory::where('id', $request->sub_category_id)->first();
        if (!$subCategory) {
            return back()->with('warning', 'Sub category not found');
        }

        SubCategory::where('id', $request->sub_category_id)->update([
            'blocked' => 0,
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        return back()->with('success', 'Sub category unblocked successfully');
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sub_category_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $subCategory = SubCategory::where('id', $request->sub_category_id)->first();
        if (!$subCategory) {
            return back()->with('warning', 'Sub category not found');
        }

        $designs = Design::where('sub_category_id', $request->sub_category_id)->get();
        if(count($designs) > 0){
            foreach ($designs as $design){
                if(Storage::disk('public')->exists('designs/' . $design->photo)){
                    Storage::disk('public')->delete('designs/' . $design->photo);
                }

                $design->delete();
            }
        }

        $subCategory->delete();

        return back()->with('success', 'Sub category deleted successfully');
    }

    public function getSubCategoriesByCategory(Request $request)
    {
        $categoryId = !empty($request->categoryId) ? trim($request->categoryId) : 0;

        $subCategories = SubCategory::where('category_id', $categoryId)
            ->where('blocked', 0)
            ->orderBy('id', 'DESC')
            ->pluck('name', 'id');

        try {
            $html = view('admin::render-select.sub-categories')->with('subCategories', $subCategories)->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">Not found any sub categories</option>']);
        }
    }

    public function getSubCategoriesByCategoryAndSelect(Request $request)
    {
        $categoryId = !empty($request->categoryId) ? trim($request->categoryId) : 0;
        $subCategoryId = !empty($request->subCategoryId) ? trim($request->subCategoryId) : 0;

        $subCategories = SubCategory::where('category_id', $categoryId)
            ->where('blocked', 0)
            ->orderBy('id', 'DESC')
            ->pluck('name', 'id');

        try {
            $html = view('admin::render-select.sub-categories')->with(['subCategories' => $subCategories, 'subCategoryId' => $subCategoryId])->render();

            return response()->json(['type' => 'success', 'html' => $html]);
        } catch (\Throwable $e) {
            Log::info($e->getMessage());

            return response()->json(['type' => 'success', 'html' => '<option value="0">Not found any sub categories</option>']);
        }
    }
}
