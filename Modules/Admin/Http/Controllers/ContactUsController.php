<?php

namespace Modules\Admin\Http\Controllers;

use App\Entities\ContactUs;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Routing\Controller;

class ContactUsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin']);
    }

    public function index()
    {
        $contactUs = ContactUs::orderBy('id', 'DESC')->paginate(10);

        return view('admin::contact-us.index', compact('contactUs'));
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'contact_us_id' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return back()->with('error', $validator->errors()->first());
        }

        $contactUs = ContactUs::where('id', $request->contact_us_id)->first();
        if (!$contactUs) {
            return back()->with('warning', 'Contact us not found');
        }

        $contactUs->delete();

        return back()->with('success', 'Contact us deleted successfully');
    }
}
